-- https://qiita.com/sonarAIT/items/0571c869e5f9ab3be817

local wezterm = require 'wezterm'

local config = {}

if wezterm.config_builder then
  config = wezterm.config_builder()
end

config.color_scheme = 'MaterialDesignColors'
config.window_background_opacity = 0.95
config.font_size = 14
config.window_background_gradient = {
        orientation = { Linear = { angle = -50.0 } },
        colors = {
                "#0f0c29",
                "#282a36",
                "#343746",
                "#3a3f52",
                "#343746",
                "#282a36",
        },
        interpolation = "Linear",
        blend = "Rgb",
        noise = 64,
        segment_size = 11,
        segment_smoothness = 1.0,
}

-- ショートカットキー設定
local act = wezterm.action

-- config.keys = {
--   -- Alt(Opt)+Shift+Fでフルスクリーン切り替え
--   {
--     key = 'f',
--     mods = 'SHIFT|META',
--     action = wezterm.action.ToggleFullScreen,
--   },
--   -- Ctrl+Shift+tで新しいタブを作成
--   {
--     key = 't',
--     mods = 'SHIFT|CTRL',
--     action = act.SpawnTab 'CurrentPaneDomain',
--   },
--   -- Ctrl+Shift+dで新しいペインを作成(画面を分割)
--   {
--     key = 'd',
--     mods = 'SHIFT|CTRL',
--     action = wezterm.action.SplitHorizontal { domain = 'CurrentPaneDomain' },
--   },
--   -- Ctrl+左矢印でカーソルを前の単語に移動
--   {
--     key = "LeftArrow",
--     mods = "CTRL",
--     action = act.SendKey {
--       key = "b",
--       mods = "META",
--     },
--   },
--   -- Ctrl+右矢印でカーソルを次の単語に移動
--   {
--     key = "RightArrow",
--     mods = "CTRL",
--     action = act.SendKey {
--       key = "f",
--       mods = "META",
--     },
--   },
--   -- Ctrl+Backspaceで前の単語を削除
--   {
--     key = "Backspace",
--     mods = "CTRL",
--     action = act.SendKey {
--       key = "w",
--       mods = "CTRL",
--     },
--   },
-- }

return config
