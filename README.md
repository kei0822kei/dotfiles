# dotfiles

This project is the collection of personal settings for zsh, neovim, git etc.


# Setup

## Build Docker Image and Run

1. Modify Dockerfile.
- Choose base image
GPU support
```sh
# FROM ubuntu:22.04

FROM nvidia/cuda:12.3.1-devel-ubuntu22.04
ENV PATH="/usr/local/cuda/bin:${PATH}"
ENV INCLUDE_PATH="/usr/local/cuda/targets/x86_64-linux/include:${INCLUDE_PATH}"
ENV LD_LIBRARY_PATH="/usr/local/cuda/targets/x86_64-linux/lib:${LD_LIBRARY_PATH}"
```

CPU only
```sh
FROM ubuntu:22.04

# FROM nvidia/cuda:12.3.1-devel-ubuntu22.04
# ENV PATH="/usr/local/cuda/bin:${PATH}"
# ENV INCLUDE_PATH="/usr/local/cuda/targets/x86_64-linux/include:${INCLUDE_PATH}"
# ENV LD_LIBRARY_PATH="/usr/local/cuda/targets/x86_64-linux/lib:${LD_LIBRARY_PATH}"
```

- Set `UID` and `GID`
```sh
UID=xxxx  # revise xxxx to `id -u $USER`
GID=xxxx  # revise xxxx to `id -g $USER`
```

2. Build Dockerfile.
```sh
docker build . -t kei0822kei/dotfiles:<version>
```

3. Run.

```sh
docker run --gpus all -it -v ~/.ssh:/home/guest/.ssh -v `pwd`:/home/guest/$(basename `pwd`) -p 9999:8888 --name <name> kei0822kei/dotfiles:<version> /usr/bin/zsh
```


## Vim setting
After you open (neo)vim first, vim plugins are installed.
Restart vim and execute the following commands.

```vim
:UpdateRemotePlugins
:CocInstall coc-snippets coc-json coc-jedi coc-db coc-pair
```


# Project Setup

## Create Project Environment

1. Check default python global

```sh
pyenv versions
```

2. Install python version project use and change pyenv global

```sh
pyenv install <version>  # if not exists
pyenv global <version>
```

3. Setup poetry virtual environment
```sh
mkdir <project> && cd <project>
poetry init
poetry env use <version>
poetry shell  # activate
exit  # deactivate
```

4. Reset pyenv global
```sh
pyenv global <default global version>
```

## Install Basic Packages and Jupyerlab Settings
```sh
poetry shell
pip install pynvim flake8 ipykernel autopep8 isort
python -m ipykernel install --user --name <project>
```


# Local Setup (FUTURE EDIT)

```
# Install
% git clone https://gitlab.com/kei0822kei/dotfiles.git
% sh dotfiles/setup.sh
% rm -rf dotfiles
```
