# ----------------------------------------------------------------------------
# Choose image.
# ----------------------------------------------------------------------------

# FROM ubuntu:22.04

FROM nvidia/cuda:11.8.0-devel-ubuntu22.04
ENV PATH="/usr/local/cuda/bin:/usr/local/bin:${PATH}"
ENV INCLUDE_PATH="/usr/local/cuda/targets/x86_64-linux/include:${INCLUDE_PATH}"
ENV LD_LIBRARY_PATH="/usr/local/cuda/targets/x86_64-linux/lib:${LD_LIBRARY_PATH}"


# ----------------------------------------------------------------------------
# Parameters.
# ----------------------------------------------------------------------------

ARG CREATE_USERNAME=keiyu
ARG UID=1000
ARG GID=1000


# ----------------------------------------------------------------------------
# Build image and set timezone and locale.
# ----------------------------------------------------------------------------

ENV DEBIAN_FRONTEND=noninteractive \
  DEBCONF_NOWARNINGS=yes \
  TZ=Asia/Tokyo \
  LANG=en_US.UTF-8 \
  LANGUAGE=en_US:en \
  LC_ALL=en_US.UTF-8
RUN apt-get update && \
  apt-get install -y locales tzdata software-properties-common && \
  locale-gen en_US.UTF-8 && \
  apt-get clean && \
  rm -rf /var/lib/apt/lists/*



# ----------------------------------------------------------------------------
# Install apt packages.
# ----------------------------------------------------------------------------

ARG BASE_NECESSARIES="cmake git htop iputils-ping make sudo tree unzip vim wget zsh"
ARG PYENV_NECESSARIES="build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev curl libncursesw5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev"
ARG TMUX_NECESSARIES="automake libevent-dev ncurses-dev bison pkg-config"
ARG OPENMPI_NECESSARIES="libopenmpi-dev openmpi-bin"
ARG ADDITIONAL_NECESSARIES="graphviz-dev ripgrep luarocks"
RUN apt-get update && apt-get install -y $BASE_NECESSARIES $PYENV_NECESSARIES $TMUX_NECESSARIES $OPENMPI_NECESSARIES $ADDITIONAL_NECESSARIES


# ----------------------------------------------------------------------------
# Install lua.
# ----------------------------------------------------------------------------

RUN (cd /usr/local/lib \
  && curl -L -R -O https://www.lua.org/ftp/lua-5.4.7.tar.gz \
  && tar zxf lua-5.4.7.tar.gz \
  && mv lua-5.4.7 lua \
  && rm lua-5.4.7.tar.gz \
  && cd lua \
  && make all test \
  && ln -s ../lib/lua/src/lua /usr/local/bin/lua \
  )


# ----------------------------------------------------------------------------
# Install latest neovim.
# ----------------------------------------------------------------------------

RUN curl -LO https://github.com/neovim/neovim-releases/releases/download/v0.10.4/nvim-linux-x86_64.appimage \
  && mv nvim-linux-x86_64.appimage nvim.appimage \
  && chmod u+x nvim.appimage \
  && ./nvim.appimage --appimage-extract \
  && ln -nsf /squashfs-root/usr/bin/nvim /usr/bin/nvim \
  && rm -rf nvim.appimage


# ----------------------------------------------------------------------------
# Install latest tmux.
# ----------------------------------------------------------------------------

RUN git clone https://github.com/tmux/tmux.git tmux \
  && (cd tmux && sh autogen.sh && ./configure && make) \
  && cp tmux/tmux /usr/bin/tmux \
  && rm -rf tmux

# install guide: https://github.com/tmux/tmux/wiki/Installing


# ----------------------------------------------------------------------------
# Install lazygit.
# ----------------------------------------------------------------------------

RUN LAZYGIT_VERSION=$(curl -s "https://api.github.com/repos/jesseduffield/lazygit/releases/latest" | \grep -Po '"tag_name": *"v\K[^"]*') \
  && curl -Lo lazygit.tar.gz "https://github.com/jesseduffield/lazygit/releases/download/v${LAZYGIT_VERSION}/lazygit_${LAZYGIT_VERSION}_Linux_x86_64.tar.gz" \
  && tar xf lazygit.tar.gz lazygit \
  && install lazygit -D -t /usr/local/bin/ \
  && rm lazygit.tar.gz lazygit


# ----------------------------------------------------------------------------
# Add user.
# ----------------------------------------------------------------------------

ENV SHELL=/usr/bin/zsh
ENV USER=$CREATE_USERNAME
RUN groupadd -fg $GID $USER && \
  useradd -m -s /usr/bin/zsh -u $UID -g $GID -G sudo $USER && \
  echo $USER:$USER | chpasswd
USER $USER
WORKDIR /home/$USER
ENV HOME /home/$USER


# ----------------------------------------------------------------------------
# Remove unnecessary files.
# ----------------------------------------------------------------------------

RUN rm -f .bash_logout .bashrc .profile


# ----------------------------------------------------------------------------
# Copy dotfiles.
# ----------------------------------------------------------------------------

ARG DOTFILES_DIR=$HOME/src/dotfiles
RUN mkdir -p $DOTFILES_DIR
COPY --chown=$USER:$USER . $DOTFILES_DIR


# ----------------------------------------------------------------------------
# Setup.
# ----------------------------------------------------------------------------

RUN sh $HOME/src/dotfiles/setup.sh
