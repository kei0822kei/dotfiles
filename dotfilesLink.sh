#!/bin/sh

# ------------------------------------------------------------------------------
# Define variables and make directories.
# ------------------------------------------------------------------------------

DOTFILES_DIR="$HOME/src/dotfiles"
NVIM_DIR="$HOME/.mydot/config/nvim"
TMUX_DIR="$HOME/.mydot/config/tmux"
ZSH_DIR="$HOME/.mydot/config/zsh"

mkdir -p "$NVIM_DIR" "$TMUX_DIR" "$ZSH_DIR"

# ------------------------------------------------------------------------------
# Link setting files.
# ------------------------------------------------------------------------------

### git
ln -nsf "$DOTFILES_DIR/git/gitconfig" "$HOME/.gitconfig"

### nvim
git clone https://github.com/kei0822kei/nvim $NVIM_DIR

### tmux
ln -nsf "$DOTFILES_DIR/tmux/tmux.conf" "$TMUX_DIR/tmux.conf"

### zsh
ln -nsf "$DOTFILES_DIR/zsh/zshenv" "$HOME/.zshenv"
ln -nsf "$DOTFILES_DIR/zsh/zshrc" "$ZSH_DIR/.zshrc"
if [ ! -e "$ZSH_DIR/settings.zsh" ]; then
  cp -f "$DOTFILES_DIR/zsh/settings.zsh" "$ZSH_DIR/settings.zsh"
fi
if [ ! -e "$ZSH_DIR/.p10k.zsh" ]; then
  cp -f "$DOTFILES_DIR/zsh/p10k.zsh" "$ZSH_DIR/.p10k.zsh"
fi
ln -nsf "$DOTFILES_DIR/zsh/rc" "$ZSH_DIR/rc"
