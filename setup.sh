#!/bin/sh

set -e

mkdir -p $HOME/src $HOME/bin
rm -f $HOME/.zcompdump

# ----------------------------------------------------------------------------
# Define variables necessary for setting up.
# ----------------------------------------------------------------------------

export XDG_CONFIG_HOME="$HOME/.mydot/config"
export XDG_CACHE_HOME="$HOME/.mydot/cache"
export XDG_DATA_HOME="$HOME/.mydot/share"
export XDG_STATE_HOME="$HOME/.mydot/state"

# ----------------------------------------------------------------------------
# Decorate strings.
# ----------------------------------------------------------------------------

decorate_strings() {
  echo "$(printf %${#1}s | sed "s/ /-/g")"
  echo "${1}"
  echo "$(printf %${#1}s | sed "s/ /-/g")"
  echo ""
}

# ----------------------------------------------------------------------------
# Install nvm and nodejs.
# ----------------------------------------------------------------------------

install_nvm_nodejs() {
  curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.40.1/install.sh | bash
  export NVM_DIR="$HOME/.mydot/config/nvm"
  [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm
  nvm install 23
}

decorate_strings "Install nvm and nodejs."
install_nvm_nodejs
echo "\nDone successfully.\n"

# ----------------------------------------------------------------------------
# Install pyenv.
# ----------------------------------------------------------------------------

install_pyenv() {
  export PYTHON_VERSIONS="3.12.7 3.11.10 3.10.15"
  export PYTHON_GLOBAL_VERSION=3.11.10
  export PYENV_ROOT="$XDG_CONFIG_HOME/pyenv"
  curl https://pyenv.run | bash
  export PATH="$PYENV_ROOT/bin:$PATH"
  eval "$(pyenv init -)"
  pyenv install -s $PYTHON_VERSIONS
  pyenv global $PYTHON_GLOBAL_VERSION
}

decorate_strings "Install pyenv."
install_pyenv
echo "\nDone successfully.\n"

# ----------------------------------------------------------------------------
# Install poetry.
# ----------------------------------------------------------------------------

install_poetry() {
  export POETRY_HOME=$XDG_CONFIG_HOME/poetry
  curl -sSL https://install.python-poetry.org | python -
  export PATH="$POETRY_HOME/bin:$PATH"
  poetry config virtualenvs.in-project true
  poetry self add poetry-plugin-shell
}

decorate_strings "Install poetry."
install_poetry
echo "\nDone successfully.\n"

# ----------------------------------------------------------------------------
# Install necessary packages.
# ----------------------------------------------------------------------------

install_packages() {
  yes | pip install pynvim ipykernel pyright ruff
}

decorate_strings "Install necessary packages."
install_packages
echo "\nDone successfully.\n"

# ----------------------------------------------------------------------------
# Install jupyterlab.
# ----------------------------------------------------------------------------

install_jupyterlab() {
  yes | pip install nglview jupyterlab jupyterlab_vim
  export JUPYTER_CONFIG_DIR="$HOME/.mydot/config/jupyter/config"
  jupyter lab --generate-config
  echo "c.ServerApp.token = ''" >>$JUPYTER_CONFIG_DIR/jupyter_lab_config.py
  echo "c.ServerApp.ip = '0.0.0.0'" >>$JUPYTER_CONFIG_DIR/jupyter_lab_config.py
  echo "c.ServerApp.open_browser = False" >>$JUPYTER_CONFIG_DIR/jupyter_lab_config.py
  echo "c.ExtensionApp.open_browser = False" >>$JUPYTER_CONFIG_DIR/jupyter_lab_config.py
}

decorate_strings "Install jupyterlab."
install_jupyterlab
echo "\nDone successfully.\n"

# ----------------------------------------------------------------------------
# Install fzf.
# ----------------------------------------------------------------------------

install_fzf() {
  git clone --depth 1 https://github.com/junegunn/fzf.git $HOME/src/fzf
  $HOME/src/fzf/install --no-bash --no-fish --bin
  cp $HOME/src/fzf/bin/* $HOME/bin
}

decorate_strings "Install fzf."
install_fzf
echo "\nDone successfully.\n"

# ----------------------------------------------------------------------------
# Install rust.
# ----------------------------------------------------------------------------

install_rust() {
  export RUST_HOME=$XDG_DATA_HOME/rust
  export RUSTUP_HOME=$RUST_HOME/rustup
  export CARGO_HOME=$RUST_HOME/cargo
  mkdir -p $RUST_HOME
  curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs >${RUST_HOME}/rustup.sh &&
    chmod +x ${RUST_HOME}/rustup.sh &&
    ${RUST_HOME}/rustup.sh -y --default-toolchain nightly --no-modify-path
}

decorate_strings "Install rust."
install_rust
echo "\nDone successfully.\n"

install_rust_packages() {
  $CARGO_HOME/bin/cargo install bat eza fd-find git-delta
}

decorate_strings "Install rust packages."
install_rust_packages
echo "\nDone successfully.\n"

# ----------------------------------------------------------------------------
# Make link.
# ----------------------------------------------------------------------------

(
  cd $(dirname $0)
  ./dotfilesLink.sh
)

# ----------------------------------------------------------------------------
# Setup has done successfully.
# ----------------------------------------------------------------------------

cat <<EOF
Everything was installed successfully.
Please logout and relogin.
Enjoy your awful terminal life!
EOF

exit 0
