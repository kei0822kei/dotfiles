#!/bin/zsh

### set variables
ZINIT_DIR="$XDG_CONFIG_HOME/zsh/zinit"
ZINIT_HOME="$XDG_CONFIG_HOME/zsh/.zinit"
typeset -gAH ZINIT
export ZINIT[HOME_DIR]="$ZINIT_HOME"
export ZINIT[BIN_DIR]="${ZINIT[HOME_DIR]}/bin"
export ZINIT[PLUGINS_DIR]="${ZINIT[HOME_DIR]}/plugins"
export ZINIT[COMPLETIONS_DIR]="${ZINIT[HOME_DIR]}/completions"
export ZINIT[SNIPPETS_DIR]="${ZINIT[HOME_DIR]}/snippets"
export ZINIT[SERVICES_DIR]="${ZINIT[HOME_DIR]}/services"
export ZINIT[ZCOMPDUMP_PATH]="${ZINIT[HOME_DIR]}/.zcompdump"
export ZPFX="${ZINIT[HOME_DIR]}/polaris"


### zinit install
if [ ! -e "$ZINIT_DIR" ]; then
  git clone https://github.com/zdharma-continuum/zinit $ZINIT_DIR
fi

### zinit setting
source $ZINIT_DIR/zinit.zsh
autoload -Uz _zinit
(( ${+_comps} )) && _comps[zinit]=_zinit

### install plugins
zinit ice depth=1
zinit light romkatv/powerlevel10k
zinit light zsh-users/zsh-autosuggestions
zinit light zdharma-continuum/fast-syntax-highlighting

### setting of each plugin
source "$XDG_CONFIG_HOME/zsh/rc/autosuggestions.zsh"
